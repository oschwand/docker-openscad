FROM debian:stretch

MAINTAINER Olivier Schwander <olivier.schwander@chadok.info>

ENV DEBIAN_FRONTEND noninteractive
ENV DISPLAY :0

# openscad fails without an X server...
RUN apt-get update && \
    apt-get install -y --no-install-recommends openscad xserver-xorg-video-dummy libgl1-mesa-dri && \
    apt-get install -y --no-install-recommends make graphicsmagick-imagemagick-compat
RUN mkdir -p /root/.local/share # openscad complains if this directory does not exist

COPY xorg.conf /etc/X11/xorg.conf
COPY startx.sh /root

